package sample.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sample.entity.Person;
import sample.repository.PersonRepo;

@Service
public class PersonService {

  private final PersonRepo personRepo;

  @Autowired
  public PersonService(PersonRepo personRepo) {
    this.personRepo = personRepo;
  }

  public List<Person> getAll() {
    return personRepo.findAll();
  }
}
