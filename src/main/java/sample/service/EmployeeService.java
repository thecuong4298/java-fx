package sample.service;

import sample.entity.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> search(String name, String phoneNumber, String address);

    void saveEmployee(Employee employee);

    void delete(Employee employee);
}
