package sample.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sample.entity.User;
import sample.repository.UserRepo;

@Service
public class UserService {

  private final UserRepo userRepo;

  @Autowired
  public UserService(UserRepo userRepo) {
    this.userRepo = userRepo;
  }

  public boolean login(String userName, String passwords) {
    Optional<User> optional = userRepo.getByUserNameAndPasswords(userName, passwords);
    return optional.isPresent();
  }
}
