package sample.config.database;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.jdbc.DataSourceBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//import sample.common.Constants;
//
//import javax.sql.DataSource;
//import java.util.HashMap;
//
////@Configuration
////@EnableJpaRepositories(
////        basePackages = "sample.repository",
////        entityManagerFactoryRef = "mmEntityManager",
////        transactionManagerRef = "mmTransactionManager")
////@EnableTransactionManagement
public class DataSourceConfig {
//
//    @Value("${spring.datasource.url}")
//    private String url;
//
//    @Value("${spring.datasource.driverClassName}")
//    private String className;
//
//    @Value("${spring.datasource.username}")
//    private String userName;
//
//    @Value("${spring.datasource.password}")
//    private String passWords;
//
//    @Bean
//    public DataSource datasource() {
//
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//        dataSourceBuilder.driverClassName(className);
//        dataSourceBuilder.url(url);
//        dataSourceBuilder.username(userName);
//        dataSourceBuilder.password(passWords);
//        return dataSourceBuilder.build();
//    }
//
//    @Bean
//    public LocalContainerEntityManagerFactoryBean mmEntityManager() {
//        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//        em.setDataSource(datasource());
//        em.setPackagesToScan(Constants.PACKAGE_ENTITY);
//        em.setPersistenceUnitName(Constants.JPA_UNIT_NAME);
//        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        em.setJpaVendorAdapter(vendorAdapter);
//        HashMap<String, Object> properties = new HashMap<>();
//
//        // JPA & Hibernate
//        properties.put("hibernate.dialect", "org.hibernate.dialect.MariaDBDialect");
//        properties.put("hibernate.show-sql", true);
//        properties.put("hibernate.jdbc.batch_size", 20);
//        properties.put("hibernate.order_inserts", true);
//        properties.put("hibernate.order_updates", true);
//        em.setJpaPropertyMap(properties);
//        em.afterPropertiesSet();
//        return em;
//    }
//
//    @Bean
//    public PlatformTransactionManager mmTransactionManager() {
//
//        JpaTransactionManager transactionManager = new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(mmEntityManager().getObject());
//        return transactionManager;
//    }
//
//    @Bean
//    public LocalContainerEntityManagerFactoryBean mmEntityManagerFactory() {
//        //JpaVendorAdapteradapter can be autowired as well if it's configured in application properties.
//        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        vendorAdapter.setGenerateDdl(false);
//
//        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
//        factory.setJpaVendorAdapter(vendorAdapter);
//        //Add package to scan for entities.
//        factory.setPackagesToScan(Constants.PACKAGE_ENTITY);
//        factory.setDataSource(datasource());
//        return factory;
//    }
//
//
}
