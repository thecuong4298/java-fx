package sample.config.exception;

public class CustomException extends Exception {

  public CustomException(String message) {
    super(message);
  }
}
