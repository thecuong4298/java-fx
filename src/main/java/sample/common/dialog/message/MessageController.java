package sample.common.dialog.message;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sample.common.dialog.BaseDialogController;
import sample.common.dialog.Dialog;
import sample.utils.Utils;

public class MessageController extends BaseDialogController {

  @FXML
  private Label content;

  @FXML
  private ImageView image;

  @FXML
  public Button btnOK;

  @FXML
  public void initialize() {
    setImage(Dialog.MESSAGE);
    btnOK.setBackground(Utils.createBackgroundImage("yes.png"));
  }

  public void setContent(String content) {
    this.content.setText(content);
  }

  public void setImage(int type) {
    String srcImg;
    switch (type) {
      case Dialog.MESSAGE:
        srcImg = Dialog.MESSAGE_ICON;
        break;
      case Dialog.ERROR:
        srcImg = Dialog.ERROR_ICON;
        break;
      case Dialog.SUCCESS:
        srcImg = Dialog.SUCCESS_ICON;
        break;
      default: srcImg = null;
    }
    this.image.setImage(new Image("file:src/main/resources/image/" + srcImg));
  }

  public void setImage(String url) {
    this.image.setImage(new Image("file:src/main/resources/image/" + url));
  }
}
