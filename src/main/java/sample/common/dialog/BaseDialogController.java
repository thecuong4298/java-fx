package sample.common.dialog;

import javafx.stage.Stage;

public class BaseDialogController {

    private int result;

    private Stage stage;

    public void setResult(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void close() {
        stage.close();
    }
}
