package sample.common.dialog;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.AbstractFxmlView;
import sample.common.dialog.confirm.ConfirmController;
import sample.common.dialog.message.MessageController;
import sample.controller.BaseController;
import sample.utils.Utils;

public class Dialog {

  public static final int MESSAGE = 0;
  public static final String MESSAGE_ICON = "information.png";
  public static final int ERROR = 1;
  public static final String ERROR_ICON = "errors.png";
  public static final int SUCCESS = 2;
  public static final String SUCCESS_ICON= "success.png";
  public static final int CONFIRM_YES = 1;
  public static final int CONFIRM_NO = 0;
  public static final int CONFIRM_TYPE_INSERT = 0;
  public static final int CONFIRM_TYPE_UPDATE = 1;
  public static final int CONFIRM_TYPE_DELETE = 2;
  private static final String CONFIRM_TYPE_INSERT_LB = "ConfirmTypeInsert";
  private static final String CONFIRM_TYPE_UPDATE_LB = "ConfirmTypeUpdate";
  private static final String CONFIRM_TYPE_DELETE_LB = "ConfirmTypeDelete";

  private static Stage stage ;

  public static void showMessageDialog(String conntent, int type, String... image) {
    try {
      Stage stage = new Stage();
      FXMLLoader loader = new FXMLLoader(Dialog.class.getResource("message/messageDialog.fxml"));
      stage.setTitle("Thông báo");
      stage.initModality(Modality.APPLICATION_MODAL);
      Scene scene = new Scene(loader.load(), 600, 130);
      stage.setScene(scene);
      stage.getIcons().add(new Image("file:src/main/resources/image/info.png"));
      MessageController controller = loader.getController();
      controller.setStage(stage);
      if(image.length > 0) {
        controller.setImage(image[0]);
      } else {
        controller.setImage(type);
      }
      controller.setContent(conntent);
      String css = Dialog.class.getResource("css/dialog.css").toExternalForm();
      scene.getStylesheets().add(css);
      stage.show();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static <T extends AbstractFxmlView> void showDialog(T clzz, Object parentController, boolean clear, Object ... obj) {
    try {
      if(stage == null) {
        stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
      }
      Scene scene;
      if(clzz.getView().getScene() == null) {
        scene = new Scene(clzz.getView());
      } else {
        scene = clzz.getView().getScene();
      }
      stage.setScene(scene);
      String className = clzz.getClass().getSimpleName();
      BaseController controller = clzz.getController();
      if(clear) {
        controller.clear();
          stage.setTitle(Utils.getLabel("commonCreate", className));
      } else {
        controller.setValue(obj[0]);
          stage.setTitle(Utils.getLabel("commonUpdate", className));
      }
      controller.setStage(stage);
      controller.setParentController((BaseController) parentController);
      stage.show();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static int showConfirmDialog(int type) {
    try {
      Stage stage = new Stage();
      FXMLLoader loader = new FXMLLoader(Dialog.class.getResource("confirm/confirm.fxml"));
      stage.setTitle("Xác nhận");
      stage.initModality(Modality.APPLICATION_MODAL);
      Scene scene = new Scene(loader.load(), 600, 130);
      stage.setScene(scene);
      stage.getIcons().add(new Image("file:src/main/resources/image/confirm.png"));
      ConfirmController controller = loader.getController();
      controller.setResult(-1);
      controller.setStage(stage);
      String content;
      switch (type) {
        case Dialog.CONFIRM_TYPE_INSERT:
          content = Dialog.CONFIRM_TYPE_INSERT_LB;
          break;
        case Dialog.CONFIRM_TYPE_UPDATE:
          content = Dialog.CONFIRM_TYPE_UPDATE_LB;
          break;
        case Dialog.CONFIRM_TYPE_DELETE:
          content = Dialog.CONFIRM_TYPE_DELETE_LB;
          break;
        default: content = null;
      }
      controller.setContent(Utils.getLabel(content));
      String css = Dialog.class.getResource("css/dialog.css").toExternalForm();
      scene.getStylesheets().add(css);
      stage.showAndWait();
      return controller.getResult();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return -1;
  }
}
