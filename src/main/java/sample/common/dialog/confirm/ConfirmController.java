package sample.common.dialog.confirm;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sample.common.dialog.BaseDialogController;
import sample.common.dialog.Dialog;
import sample.utils.Utils;

public class ConfirmController extends BaseDialogController {

  @FXML
  private Label content;

  @FXML
  private ImageView image;

  @FXML
  private Button btnYes;

  @FXML
  private Button btnNo;

  @FXML
  public void initialize() {
    setImage(Dialog.MESSAGE);
    btnYes.setBackground(Utils.createBackgroundImage("yes.png"));
    btnNo.setBackground(Utils.createBackgroundImage("cancel.png"));

  }

  public void setContent(String content) {
    this.content.setText(content);
  }

  public void setImage(int type) {
    this.image.setImage(new Image("file:src/main/resources/image/question.png"));
  }

  public void setImage(String url) {
    this.image.setImage(new Image("file:src/main/resources/image/" + url));
  }

  public void chooseYes() {
    setResult(Dialog.CONFIRM_YES);
    close();
  }

  public void chooseNo() {
    setResult(Dialog.CONFIRM_NO);
    close();
  }
}
