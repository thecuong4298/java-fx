package sample.common;

public class Constants {
    public static final String JPA_UNIT_NAME = "JPA_UNIT_NAME";
    public static final String PACKAGE_ENTITY = "sample.entity";
    // image
    public static final String IMAGE_DELETE = "delete.png";
    public static final String IMAGE_EDIT = "edit.png";
    public static final String IMAGE_SEARCH = "search.png";
}
