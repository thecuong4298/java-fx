package sample.common;

import sample.view.EmployeeView;

import java.lang.reflect.Field;
import java.util.HashMap;

public class Common {
    private static String employeeView = "Nhân viên";
    private static String employeeDialogView = " nhân viên";
    private static String titleApp = "Quản lý doanh nghiệp";
    private static String commonCode = "Mã nhân viên";
    private static String commonFullName = "Họ tên đầy đủ";
    private static String commonAddress = "Địa chỉ";
    private static String commonCreateDate = "Ngày tạo";
    private static String commonCreate = "Thêm mới";
    private static String commonUpdate = "Cập nhật";
    private static String commonInsertSuccess = "Thêm mới thành công";
    private static String commonUpdateSuccess = "Cập nhật thành công";
    private static String commonErrors = "Có lỗi xảy ra";
    private static String confirmTypeInsert = "Bạn có chắc chắn muốn thêm mới không?";
    private static String confirmTypeUpdate = "Bạn có chắc chắn muốn cập nhật không?";
    private static String confirmTypeDelete = "Bạn có chắc chắn muốn xóa không?";
    public static String commonEmpty = " không được để trống";
    private static String code = "Mã nhân viên";
    private static String fullName = "Họ và tên";
    private static String phoneNumber = "Số điện thoại";
    private static String address = "Địa chỉ";
    private static String dateOfBirth = "Ngày sinh";
    private static String startWork = "Ngày bắt đầu";

    public static HashMap<String, String> getAllLabel() {
        HashMap<String, String> hmap = new HashMap<>();
        try {
            Field[] fields =  Common.class.getDeclaredFields();
            for(Field field: fields) {
                hmap.put(field.getName().toLowerCase(), (String) field.get(Common.class));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return hmap;
    }
}
