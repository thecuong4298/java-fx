package sample;

import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import sample.config.AbstractJavaFxApplicationSupport;
import sample.utils.Utils;
import sample.view.ContainerView;
import sample.view.LoginView;

import java.awt.*;
import java.io.IOException;

@SpringBootApplication
@SuppressWarnings("restriction")
public class Main extends AbstractJavaFxApplicationSupport {

    @Autowired
    private LoginView loginView;

    @Autowired
    private ContainerView containerView;

    @Value("classpath:/image/icon-app.png")
    private Resource resource;

    @Override
    public void start(Stage primaryStage) throws IOException {
        notifyPreloader(new Preloader.StateChangeNotification(Preloader.StateChangeNotification.Type.BEFORE_START));
        primaryStage.setTitle(Utils.getLabel("TitleApp"));
        primaryStage.getIcons().add(new Image(resource.getInputStream()));
        primaryStage.setScene(new Scene(containerView.getView()));
//        String css = this.getClass().getResource("view/css/tab-pane.css").toExternalForm();
//        Utils.getTabPane().getStylesheets().add(css);
        primaryStage.show();
        Utils.stage = primaryStage;
    }

    public static void main(String[] args) {

        System.setProperty("java.awt.headless", "read");
        Toolkit tk = Toolkit.getDefaultToolkit();
        // Standard beep is available.
        tk.beep();
        GraphicsEnvironment ge =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        launchApp(Main.class, args);
    }
}
