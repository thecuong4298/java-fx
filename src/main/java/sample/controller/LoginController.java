package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sample.common.dialog.Dialog;
import sample.service.UserService;
import sample.utils.Utils;
import sample.view.ContainerView;

@Component
public class LoginController extends BaseController {

  @FXML
  private Button btnLogin;
  @FXML
  private TextField txtUserName;
  @FXML
  private PasswordField txtPasswords;

  private final ContainerView containerView;
  private final UserService userService;

  @Autowired
  public LoginController(ContainerView containerView, UserService userService) {
    this.containerView = containerView;
    this.userService = userService;
  }

  public void login() {
    if (userService.login(txtUserName.getText(), txtPasswords.getText())) {
      Utils.navigateView(containerView, true);
    } else {
      Dialog.showMessageDialog("Tài khoản hoặc mật khẩu không đúng!", Dialog.ERROR);
    }
  }
}
