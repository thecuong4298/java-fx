package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sample.utils.Utils;
import sample.view.EmployeeView;
import sample.view.LoginView;

import java.util.Date;

@Component
public class ContainerController extends BaseController {

    private final LoginView loginView;
    private final EmployeeView employeeView;

    @FXML
    private BorderPane borderPane;

    @FXML
    private TabPane tabPane;

    public TabPane getTabPaneInstance() {
        return tabPane;
    }

    @Autowired
    public ContainerController(LoginView loginView, EmployeeView employeeView) {
        this.loginView = loginView;
        this.employeeView = employeeView;
    }

    public void loadViewEmployee() {
        Utils.addTabToPane(employeeView);
    }
}
