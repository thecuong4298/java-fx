package sample.controller;

import javafx.stage.Stage;

public class BaseController {

    private Stage stage;
    private BaseController parentController;

    public void setParentController(BaseController parentController) {
        this.parentController = parentController;
    }

    public void onEdit(Object obj) {

    }

    public void onDelete(Object obj) {

    }

    public void onCloseDialog() {

    }

    public void clear() {

    }

    public void setValue(Object obj) {

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void close() {
        stage.close();
        parentController.onCloseDialog();
    }
}
