package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sample.common.Common;
import sample.common.dialog.Dialog;
import sample.config.FxDatePickerConverter;
import sample.entity.Employee;
import sample.repository.EmployeeRepo;
import sample.service.EmployeeService;
import sample.utils.Utils;
import sample.utils.ValidateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class EmployeeDialogController extends BaseController {

    @FXML
    private TextField txtCode;

    @FXML
    private TextField txtFullName;

    @FXML
    private TextField txtPhoneNumber;

    @FXML
    private TextField txtAddress;

    @FXML
    private DatePicker txtDateOfBirth;

    @FXML
    private DatePicker txtStartWork;

    @FXML
    private TextArea txtDescription;

    @FXML
    private Button btnReset;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnSave;

    private EmployeeService employeeService;

    private Long employeeId;

    private List<TextField> lstControl;

    private List<DatePicker> lstDatePicker;

    @Autowired
    public EmployeeDialogController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @FXML
    public void initialize() {
        btnCancel.setBackground(Utils.createBackgroundImage("cancel.png"));
        btnReset.setBackground(Utils.createBackgroundImage("reset.png"));
        btnSave.setBackground(Utils.createBackgroundImage("save.png"));
        txtDateOfBirth.setConverter(new FxDatePickerConverter());
        txtStartWork.setConverter(new FxDatePickerConverter());
        lstControl = new ArrayList<>();
        lstControl.add(txtCode);
        lstControl.add(txtFullName);
        lstControl.add(txtPhoneNumber);
        lstControl.add(txtAddress);
        lstDatePicker = new ArrayList<>();
        lstDatePicker.add(txtDateOfBirth);
        lstDatePicker.add(txtStartWork);
    }

    public void insert() {
        if (!ValidateUtils.validTextField(lstControl) || !ValidateUtils.validDatePicker(lstDatePicker)
                || Dialog.CONFIRM_YES != Dialog.showConfirmDialog(employeeId == null ? Dialog.CONFIRM_TYPE_INSERT : Dialog.CONFIRM_TYPE_UPDATE)) {
            return;
        }
        try {
            Employee employee = new Employee();
            employee.setCode(txtCode.getText());
            employee.setFullName(txtFullName.getText());
            employee.setPhoneNumber(txtPhoneNumber.getText());
            employee.setAddress(txtAddress.getText());
            employee.setDescriptions(txtDescription.getText());
            employee.setCreateDate(new Date());
            employee.setDateOfBirth(Utils.convertDate(txtDateOfBirth.getEditor().getText()));
            employee.setStartWork(Utils.convertDate(txtStartWork.getEditor().getText()));
            employee.setId(employeeId);
            employeeService.saveEmployee(employee);
            Dialog.showMessageDialog(employeeId == null ? Utils.getLabel("CommonInsertSuccess")
                    : Utils.getLabel("commonUpdateSuccess"), Dialog.SUCCESS);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Dialog.showMessageDialog(Utils.getLabel("commonErrors"), Dialog.ERROR);
        }
    }

    @Override
    public void clear() {
        employeeId = null;
        txtCode.requestFocus();
        txtCode.clear();
        txtFullName.clear();
        txtPhoneNumber.clear();
        txtAddress.clear();
        txtDescription.clear();
        txtDateOfBirth.getEditor().clear();
        txtStartWork.getEditor().clear();
    }

    @Override
    public void setValue(Object obj) {
        Employee employee = (Employee) obj;
        txtCode.requestFocus();
        txtCode.setText(employee.getCode());
        txtFullName.setText(employee.getFullName());
        txtPhoneNumber.setText(employee.getPhoneNumber());
        txtAddress.setText(employee.getAddress());
        txtDescription.setText(employee.getDescriptions());
        txtDateOfBirth.getEditor().setText(Utils.convertDateToString(employee.getDateOfBirth()));
        txtStartWork.getEditor().setText(Utils.convertDateToString(employee.getStartWork()));
        employeeId = employee.getId();
    }
}
