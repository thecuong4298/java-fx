package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sample.common.dialog.Dialog;
import sample.entity.Employee;
import sample.repository.EmployeeRepo;
import sample.service.EmployeeService;
import sample.utils.Utils;
import sample.view.EmployeeDialogView;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeController extends BaseController {

    @FXML
    private TextField txtName;
    @FXML
    private TextField txtPhoneNumber;
    @FXML
    private TextField txtAddress;
    @FXML
    private Button btnInsert;
    @FXML
    private Button btnSearch;

    @FXML
    private TableView<Employee> tblEmployee;

    private EmployeeService employeeService;

    private EmployeeDialogView employeeDialogView;

    private boolean setCellFactory = false;

    @Autowired
    public EmployeeController(EmployeeService employeeService, EmployeeDialogView employeeDialogView) {
        this.employeeService = employeeService;
        this.employeeDialogView = employeeDialogView;
    }

    @FXML
    public void initialize() {
        btnSearch.setBackground(Utils.createBackgroundImage("search.png"));
        btnInsert.setBackground(Utils.createBackgroundImage("add.png"));
    }

    private void setPropertyValueFactory() {
        if(setCellFactory) {
            return;
        }
        ObservableList<TableColumn<Employee, ?>> lstColumn = tblEmployee.getColumns();
        for(TableColumn column: lstColumn) {
            column.setCellValueFactory(new PropertyValueFactory(column.getId()));
        }
        Utils.addButtonToTable(tblEmployee, this);
        setCellFactory = true;
    }

    public void search() {
        setPropertyValueFactory();
        List<Employee> lst = employeeService.search(txtName.getText(), txtPhoneNumber.getText(), txtAddress.getText());
        tblEmployee.setItems(FXCollections.observableArrayList(lst));
    }

    public void openDialog() {
        Dialog.showDialog(employeeDialogView, this, true);
    }

    @Override
    public void onEdit(Object obj) {
        Dialog.showDialog(employeeDialogView, this, false, obj);
    }

    @Override
    public void onCloseDialog() {
        search();
    }

    @Override
    public void clear() {
        txtPhoneNumber.clear();
        txtName.clear();
        txtAddress.clear();
        tblEmployee.setItems(FXCollections.observableArrayList(new ArrayList<>()));
        txtName.requestFocus();
    }

    @Override
    public void onDelete(Object obj) {
        if(Dialog.CONFIRM_YES == Dialog.showConfirmDialog(Dialog.CONFIRM_TYPE_DELETE)) {
            employeeService.delete((Employee) obj);
            search();
        }
    }
}
