package sample.utils;

import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import sample.common.Common;
import sample.common.dialog.Dialog;

import java.util.List;

public class ValidateUtils {
    public static boolean validTextField(List<TextField> lstControl) {
        for(TextField control: lstControl) {
            if(Utils.isNullOrEmpty(control.getText())) {
                Dialog.showMessageDialog(Utils.getLabel(control.getId()) + Common.commonEmpty,
                        Dialog.ERROR);
                control.requestFocus();
                return false;
            }
        }
        return true;
    }
    public static boolean validDatePicker(List<DatePicker> lstControl) {
        for(DatePicker control: lstControl) {
            if(Utils.isNullOrEmpty(control.getEditor().getText())) {
                Dialog.showMessageDialog(Utils.getLabel(control.getId()) + Common.commonEmpty,
                        Dialog.ERROR);
                control.requestFocus();
                return false;
            }
        }
        return true;
    }
}
