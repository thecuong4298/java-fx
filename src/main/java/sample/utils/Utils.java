package sample.utils;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import sample.AbstractFxmlView;
import sample.common.Common;
import sample.common.Constants;
import sample.config.ApplicationContextHolder;
import sample.controller.BaseController;
import sample.controller.ContainerController;
import sample.entity.Employee;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

  public static Stage stage;
  private static TabPane tabPane;
  private static ObservableList<Tab> listTab;
  private static HashMap<String, String> hmapLabel;
  private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

  public static TabPane getTabPane() {
    if(tabPane == null) {
      synchronized(TabPane.class) {
        if(null == tabPane) {
          tabPane = ApplicationContextHolder.getContext().getBean(ContainerController.class).getTabPaneInstance();
        }
      }
    }
    return tabPane;
  }

  public static ObservableList<Tab> getInstanceListTab() {
    if(tabPane == null) {
      getTabPane();
    }
    listTab = tabPane.getTabs();
    return listTab;
  }

  public static String getLabel(String... keys) {
    if(hmapLabel == null) {
      synchronized(TabPane.class) {
        if(null == hmapLabel) {
          hmapLabel = Common.getAllLabel();
        }
      }
    }
    StringBuilder label = new StringBuilder();
    for(String key: keys) {
      label.append(hmapLabel.get(key.toLowerCase()));
    }
    return label.toString();
  }

  public static <T extends AbstractFxmlView> void navigateView(T clazz, Boolean... reload) {
    if(reload.length > 0) {
      stage.close();
      stage = new Stage();
    }
    stage.setScene(new Scene(clazz.getView()));
    stage.show();
  }

  public static <T extends AbstractFxmlView> void addTabToPane(T clazz) {
    String className = clazz.getClass().getSimpleName();
    List<Tab> lst = getInstanceListTab().stream().filter(tab-> className.equals(tab.getId())).collect(Collectors.toList());
    Tab tab =  new Tab(getLabel(className));
    if(lst.isEmpty()) {
      tab.setContent(clazz.getView());
      tab.setId(className);
      tab.setOnCloseRequest( event -> onCloseTab());
      clazz.getController().clear();
      listTab.add(tab);
    } else {
      tab = lst.get(0);
    }
    tabPane.getSelectionModel().select(tab);
  }

  private static void onCloseTab() {
    System.out.println("close");
  }

  public static TableColumn createColumn(String key, String field) {
    TableColumn column = new TableColumn(getLabel(key));
    column.setCellValueFactory(new PropertyValueFactory(field));
    return column;
  }

  public static void autoResizeColumns( TableView<?> table )
  {
    //Set the right policy
    table.setColumnResizePolicy( TableView.UNCONSTRAINED_RESIZE_POLICY);
    table.getColumns().stream().forEach( (column) ->
    {
      //Minimal width = columnheader
      Text t = new Text( column.getText() );
      double max = t.getLayoutBounds().getWidth();
      for ( int i = 0; i < table.getItems().size(); i++ )
      {
        //cell must not be empty
        if ( column.getCellData( i ) != null )
        {
          t = new Text( column.getCellData( i ).toString() );
          double calcwidth = t.getLayoutBounds().getWidth();
          //remember new max-width
          if ( calcwidth > max )
          {
            max = calcwidth;
          }
        }
      }
      //set the new max-widht with some extra space
      column.setPrefWidth( max + 10.0d );
    } );
  }

  public static <T extends BaseController> void addButtonToTable(TableView tableView, T clzz) {
    ObservableList<TableColumn> lstColumn = tableView.getColumns();
    List<TableColumn> array = lstColumn.stream().filter(column -> "action".equals(column.getId())).collect(Collectors.toList());
    if(array.isEmpty()) {
      return;
    }

    Callback<TableColumn, TableCell> cellFactory = new Callback<TableColumn, TableCell>() {
      @Override
      public TableCell call(final TableColumn param) {
        final TableCell cell = new TableCell<Object, Void>() {
          private Button btnEdit = new Button();
          {
            btnEdit.setOnAction((ActionEvent event) -> {
              Object obj = getTableView().getItems().get(getIndex());
              clzz.onEdit(obj);
            });
            btnEdit.setPrefWidth(32);
            btnEdit.setLayoutX(20);
            btnEdit.setBackground(createBackgroundImage(Constants.IMAGE_EDIT));
            btnEdit.setCursor(Cursor.HAND);
          }
          private final Button btnDelete = new Button();
          {
            btnDelete.setOnAction((ActionEvent event) -> {
              Object obj = getTableView().getItems().get(getIndex());
              clzz.onDelete(obj);
            });
            btnDelete.setBackground(createBackgroundImage(Constants.IMAGE_DELETE));
            btnDelete.setPrefWidth(32);
            btnDelete.setLayoutX(80);
          }

          @Override
          public void updateItem(Void item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
              setGraphic(null);
            } else {
              Pane pane = new Pane();
              pane.getChildren().add(btnEdit);
              pane.getChildren().add(btnDelete);
              setGraphic(pane);
            }
          }
        };
        return cell;
      }
    };
    array.get(0).setCellFactory(cellFactory);
  }

  public static Background createBackgroundImage(String url) {
    BackgroundImage bg = new BackgroundImage(new Image("file:src/main/resources/image/" + url), BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT, null, null);
    return new Background(bg);
  }

  public static boolean isNullOrEmpty(String value) {
    return value == null || "".equals(value.trim());
  }

  public static Date convertDate(String date) {
    try{
      return df.parse(date);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String convertDateToString(Date date) {
    try{
      return df.format(date);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
