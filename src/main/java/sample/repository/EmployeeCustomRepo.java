package sample.repository;

import sample.entity.Employee;

import java.util.List;

public interface EmployeeCustomRepo {
    public List<Employee> getEmployeeByCondition(String name, String phoneNumber, String address);
}
