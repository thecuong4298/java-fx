package sample.repository.impl;

import org.springframework.jdbc.object.SqlQuery;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import sample.common.Constants;
import sample.entity.Employee;
import sample.repository.EmployeeCustomRepo;
import sample.utils.Utils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;

@Repository
public class EmployeeCustomRepoImpl implements EmployeeCustomRepo {

    @PersistenceContext
    private EntityManager mmEntityManager;

    @Override
    public List<Employee> getEmployeeByCondition(String name, String phoneNumber, String address) {
        StringBuilder sql = new StringBuilder();
        sql.append("From Employee where 1=1");
        HashMap<String, String> hmap = new HashMap<>();
        if (!Utils.isNullOrEmpty(name)) {
            sql.append(" and fullName LIKE CONCAT('%',:name,'%')");
            hmap.put("name", name);
        }
        if (!Utils.isNullOrEmpty(phoneNumber)) {
            sql.append(" and phoneNumber LIKE CONCAT('%',:phoneNumber,'%')");
            hmap.put("phoneNumber", phoneNumber);
        }
        if (!Utils.isNullOrEmpty(address)) {
            sql.append(" and address LIKE CONCAT('%',:address,'%')");
            hmap.put("address", address);
        }
        Query query = mmEntityManager.createQuery(sql.toString(), Employee.class);
        hmap.forEach(query::setParameter);
        return query.getResultList();
    }
}
