package sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sample.entity.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long>, EmployeeCustomRepo {
}
