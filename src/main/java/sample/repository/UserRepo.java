package sample.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import sample.entity.User;

public interface UserRepo extends JpaRepository<User, Long> {

  Optional<User> getByUserNameAndPasswords(String username, String passwords);
}
